# Chatbot with GPT-3.5
This repository contains a simple AI chatbot that uses the GPT-3.5 API to generate responses to user input.  
  

# Installation
To install this project, follow these steps:  
  
Clone the repository to your local machine  
Install the required packages by running 

    sudo ./install_packges.sh

  
Set up your GPT-3.5 API credentials by creating a .env file and adding your OpenAI API key:
 
    OPENAI_API_KEY=[your API key here]
     
# Usage
To run the chatbot, follow these steps:  

Open a terminal window and navigate to the project directory  
Run the command   

    python main.py

The chatbot will generate a random input prompt, and then display the generated response.  
Here's an example of how to run the program:  

    python main.py

# Limitations
This chatbot is a simple proof-of-concept, and may not be suitable for production use. It only generates responses based on pre-trained models and does not learn from user input over time.  

# Contact Information
If you have any questions or feedback about this project, please contact [lihikopgithub@outlook.com].

# License
This project is licensed under the MIT license. See the LICENSE file for details.  
  
I hope this example is helpful to you! Remember to customize it to fit your specific project and audience.